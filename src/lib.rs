pub mod search;

use crate::search::SearchEntry;
use bytes::Bytes;
use select::{document::Document, predicate::*};
use serde::{Deserialize, Serialize};
use std::{borrow::Cow, collections::HashMap};
use url::Url;

pub type AnimeResult<T> = Result<T, AnimeError>;

#[derive(Debug)]
pub enum AnimeError {
    Reqwest(reqwest::Error),
    InvalidStatus(reqwest::StatusCode),
    Json(serde_json::Error),

    MissingAnimeList,

    MissingVideoList,

    UnsupportedVideoBackend,

    VideoProviderError,
}

impl From<reqwest::Error> for AnimeError {
    fn from(e: reqwest::Error) -> Self {
        Self::Reqwest(e)
    }
}

impl From<serde_json::Error> for AnimeError {
    fn from(e: serde_json::Error) -> Self {
        Self::Json(e)
    }
}

#[derive(Clone)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::new(),
        }
    }

    pub async fn search(&self, query: &str) -> AnimeResult<Vec<SearchEntry>> {
        let url = format!(
            "https://watchanime.info/wp-json/wp/v2/search?search={}",
            query
        );
        let res = self.client.get(&url).send().await?;

        let status = res.status();
        if !status.is_success() {
            return Err(AnimeError::InvalidStatus(status));
        }

        let data = res.bytes().await?;
        let ret = serde_json::from_slice(&data)?;

        Ok(ret)
    }

    pub async fn get_episode_urls(&self, url: &Url) -> AnimeResult<Vec<Url>> {
        let res = self.client.get(url.as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(AnimeError::InvalidStatus(status));
        }
        let data = res.text().await?;
        let doc = Document::from(data.as_str());

        let ret = doc
            .find(Class("episode-list"))
            .next()
            .ok_or(AnimeError::MissingAnimeList)?
            .find(Class("card-body"))
            .next()
            .ok_or(AnimeError::MissingAnimeList)?
            .find(Name("a"))
            .filter_map(|el| el.attr("href"))
            .map(|l| Url::parse(l).ok())
            .collect::<Option<Vec<_>>>()
            .ok_or(AnimeError::MissingAnimeList)?;

        Ok(ret)
    }

    pub async fn get_video_provider_url(&self, url: &Url) -> AnimeResult<Vec<Url>> {
        let res = self.client.get(url.as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(AnimeError::InvalidStatus(status));
        }
        let data = res.text().await?;
        let doc = Document::from(data.as_str());
        let ret = doc
            .find(Class("link-video"))
            .flat_map(|el| {
                el.find(Name("input"))
                    .filter_map(|el| el.attr("value"))
                    .map(|el| el.trim())
                    .filter(|el| !el.is_empty())
            })
            .map(unambiguate_url)
            .map(|l| Url::parse(&l).ok())
            .collect::<Option<Vec<_>>>()
            .ok_or(AnimeError::MissingVideoList)?;

        Ok(ret)
    }

    pub async fn download_video(&self, url: &Url) -> AnimeResult<Bytes> {
        match url.host_str() {
            Some("gcloud.live") => {
                let id = url
                    .path_segments()
                    .and_then(|p| p.last())
                    .ok_or(AnimeError::VideoProviderError)?;
                let res = self
                    .client
                    .post(&format!("https://gcloud.live/api/source/{}", id))
                    .form(&[("r", ""), ("g", "dcloud.live")])
                    .send()
                    .await?;
                let status = res.status();
                if !status.is_success() {
                    return Err(AnimeError::InvalidStatus(status));
                }
                let data = res.bytes().await?;
                let video_info: GCloudVideoInfo = serde_json::from_slice(&data)?;
                let url = &video_info
                    .data
                    .iter()
                    .last()
                    .ok_or(AnimeError::VideoProviderError)?
                    .file;
                let res = self.client.get(url.as_str()).send().await?;
                let status = res.status();
                if !status.is_success() {
                    return Err(AnimeError::InvalidStatus(status));
                }
                let data = res.bytes().await?;
                Ok(data)
            }
            _ => {
                return Err(AnimeError::UnsupportedVideoBackend);
            }
        }
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

fn unambiguate_url(url: &str) -> Cow<str> {
    if url.starts_with("//") {
        format!("https:{}", url).into()
    } else {
        url.into()
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct GCloudVideoInfo {
    data: Vec<GCloudVideoSource>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct GCloudVideoSource {
    label: String,
    file: Url,
    #[serde(rename = "type")]
    video_type: String,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
