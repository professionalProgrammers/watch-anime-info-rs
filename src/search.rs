use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use url::Url;

#[derive(Debug, Deserialize, Serialize)]
pub struct SearchEntry {
    id: u64,
    #[serde(rename = "subtype")]
    subkind: Kind,
    title: String,
    url: Url,
    #[serde(rename = "type")]
    kind: Kind,
    #[serde(rename = "_links")]
    extra_links: ExtraLinks,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl SearchEntry {
    pub fn id(&self) -> u64 {
        self.id
    }

    pub fn subkind(&self) -> &Kind {
        &self.subkind
    }

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn url(&self) -> &Url {
        &self.url
    }

    pub fn kind(&self) -> &Kind {
        &self.kind
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ExtraLinks {
    about: Vec<Link>,
    collection: Vec<Link>,
    #[serde(rename = "self")]
    this: Vec<Link>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Link {
    embeddable: Option<bool>,
    href: Url,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum Kind {
    #[serde(rename = "post")]
    Post,

    #[serde(rename = "watch")]
    Watch,
}
